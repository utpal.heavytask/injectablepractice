import 'package:auto_injector_demo/src/data/network/network_module.dart';
import 'package:auto_injector_demo/src/presentation/features/home/home_screen.dart';
import 'package:auto_injector_demo/src/presentation/injection/injector.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await NetworkModule.inject();
  configureInjections();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Injector Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: const HomeScreen(),
    );
  }
}
