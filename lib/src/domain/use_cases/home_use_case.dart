import 'package:auto_injector_demo/src/domain/common/use_case.dart';
import 'package:auto_injector_demo/src/domain/services/post_count_service.dart';
import 'package:injectable/injectable.dart';

@Injectable()
class HomeUseCase extends UseCase<String, Future> {
  final PostCountService _postCountService;

  HomeUseCase(this._postCountService);
  @override
  Future<List> run(input) async {
    return await _postCountService.getPostCount();
  }
}
