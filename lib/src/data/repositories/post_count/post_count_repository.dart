import 'package:auto_injector_demo/src/data/repositories/post_count/source/remote/post_count_remote_data_source.dart';
import 'package:auto_injector_demo/src/domain/services/post_count_service.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: PostCountService)
class PostCountRepository extends PostCountService {
  final PostCountRemoteDataSource _postCountDataSource;

  PostCountRepository(this._postCountDataSource);
  @override
  Future getPostCount() async {
    // response is too fast :p so added some delay
    // just to test loading indicator is visible while loading
    //await Future.delayed(const Duration(seconds: 1));
    return await _postCountDataSource.getPosts();
  }
}
