import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'post_count_remote_data_source.g.dart';

@RestApi()
@Injectable()
abstract class PostCountRemoteDataSource {
  @factoryMethod
  factory PostCountRemoteDataSource(Dio dio) = _PostCountRemoteDataSource;

  @GET('/posts')
  Future getPosts();
}
