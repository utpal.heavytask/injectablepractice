import 'package:auto_injector_demo/src/presentation/injection/injector.dart';
import 'package:dio/dio.dart';

class NetworkModule {
  static Future inject() async {
    injector.registerLazySingleton<Dio>(
      () {
        final dio = Dio();
        dio.options.baseUrl = "https://jsonplaceholder.typicode.com";
        return dio;
      },
    );
  }
}
