import 'package:auto_injector_demo/src/domain/use_cases/home_use_case.dart';
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'home_state.dart';
part 'home_cubit.freezed.dart';

@Injectable()
class HomeCubit extends Cubit<HomeState> {
  final HomeUseCase homeUseCase;
  HomeCubit(this.homeUseCase) : super(const HomeState.initial());

  getData() {
    try {
      emit(const HomeState.loading());
      homeUseCase
          .run('')
          .then((value) => emit(HomeState.loaded(value.length.toString())));
    } catch (e) {
      e is DioError
          ? emit(const HomeState.failure("Failed"))
          : emit(const HomeState.initial());
    }
  }
}
