import 'package:auto_injector_demo/src/presentation/features/cubit/home_cubit.dart';
import 'package:auto_injector_demo/src/presentation/injection/injector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Injector + Dio + Retrofit"),
      ),
      body: BlocProvider(
        create: (context) => injector.get<HomeCubit>(),
        child: const Homebody(),
      ),
    );
  }
}

class Homebody extends StatelessWidget {
  const Homebody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          BlocBuilder<HomeCubit, HomeState>(
            builder: (context, state) {
              return state.when(
                  initial: () => const Text(
                        "0",
                        style: TextStyle(
                          fontSize: 64,
                        ),
                      ),
                  loading: () => const Center(
                        child: CircularProgressIndicator(),
                      ),
                  loaded: (value) => Text(
                        value,
                        style: const TextStyle(
                          fontSize: 64,
                          color: Colors.green,
                        ),
                      ),
                  failure: (err) => const Text("Failed"));
            },
          ),
          const SizedBox(height: 24),
          ElevatedButton(
            onPressed: () {
              context.read<HomeCubit>().getData();
            },
            child: const Text("Get data"),
          ),
        ],
      ),
    );
  }
}
