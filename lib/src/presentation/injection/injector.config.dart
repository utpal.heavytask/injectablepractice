// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:dio/dio.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../data/repositories/post_count/post_count_repository.dart' as _i6;
import '../../data/repositories/post_count/source/remote/post_count_remote_data_source.dart'
    as _i3;
import '../../domain/services/post_count_service.dart' as _i5;
import '../../domain/use_cases/home_use_case.dart' as _i7;
import '../features/cubit/home_cubit.dart'
    as _i8; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.PostCountRemoteDataSource>(
      () => _i3.PostCountRemoteDataSource(get<_i4.Dio>()));
  gh.factory<_i5.PostCountService>(
      () => _i6.PostCountRepository(get<_i3.PostCountRemoteDataSource>()));
  gh.factory<_i7.HomeUseCase>(
      () => _i7.HomeUseCase(get<_i5.PostCountService>()));
  gh.factory<_i8.HomeCubit>(() => _i8.HomeCubit(get<_i7.HomeUseCase>()));
  return get;
}
