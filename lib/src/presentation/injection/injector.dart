import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'injector.config.dart';

GetIt injector = GetIt.instance;

@injectableInit
configureInjections() => $initGetIt(injector);
